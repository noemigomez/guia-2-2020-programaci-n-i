#!/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def ordenar(dic):
    nuevo = {}
    # molecula a str
    molecula = list(dic.values())[0]
    # agregar terminos a diccionario nuevo, de 2 en 2
    for i in range(1, len(molecula), 2):
        # empieza en indice 1, tomando 0 y 1, siendo este C6
        atom = molecula[i - 1] + molecula[i]
        novo = {atom: 'Histidina'}
        nuevo.update(novo)
    return nuevo


if __name__ == "__main__":
    dic = {'Histidina': 'C6H9N3O2'}
    nuevo = ordenar(dic)
    print(dic)
    print(nuevo)
