#!/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# imprime los datos solicidato
def imprimir_datos(paises, infected, dead_recov):
    print('Los países con almenos un contagiado son {0}'.format(len(paises)))
    for i in range(len(paises)):
        enunciado = '{0}: {1} contagiados y {2} recuperados + muertos.'
        # separado debido a que superaba los 79 caracteres
        print(enunciado.format(paises[i], infected[i], dead_recov[i]))


def infec_dead_recov(paises):
    # lista de infectados y muertos + recuperados
    infected = []
    dead_recov = []
    for h in paises:
        archivo = open('covid_19_data.csv')
        # contadores para cada lista
        cont_i = 0
        cont_d_r = 0
        for i, linea in enumerate(archivo):
            linea = linea.split(',')
            if linea[1] == '03/27/2020':
                # si el pais se encuentra, el contador suma sus contagiados
                if h == linea[3]:
                    cont_i += float(linea[5])
                    cont_d_r += (float(linea[6]) + float(linea[7]))
        infected.append(cont_i)
        dead_recov.append(cont_d_r)
    archivo.close()
    return infected, dead_recov


# revisa si el país que recibe ya está en la lista
def revisar(pais, paises):
    cont = 0
    for i in range(len(paises)):
        if paises[i] == pais:
            cont += 1
    return cont


# crea listas de países presentes en el archivo
def paises_contag():
    archivo = open('covid_19_data.csv')
    paises = []
    for i, linea in enumerate(archivo):
        # columnas separadas por ,
        linea = linea.split(',')
        # la primera linea no tiene ningún dato útil
        if i > 0:
            # ultima fecha de actualización
            if linea[1] == '03/27/2020':
                # revisa si el pais ya está en la lista
                si = revisar(linea[3], paises)
                # si no está, se agrega
                if si == 0:
                    paises.append(linea[3])
    archivo.close()
    return paises


# entrega de datos de covid por país
def estadisticas():
    paises = paises_contag()
    # dead:muertos, recov de recovered: recuperados
    infected, dead_recov = infec_dead_recov(paises)
    imprimir_datos(paises, infected, dead_recov)


if __name__ == "__main__":
    print('CASOS DE COVID EN EL MUNDO, 27 DE MARZO 2020')
    estadisticas()
