def comprobare(aa):
    # convierte en lista el key de aa
    aa1 = list(aa)
    aminoacidos = {"HISTIDINA": "C6H9N3O2", "ISOLEUCINA": "C6H13NO2",
                   "LEUCINA": "C6H13NO2", "LISINA": "C6H14N2O2",
                   "METIONINA": "C5H11NO2S","FENILALANINA":"C9H11NO2",
                   "TREONINA": "C4H9NO3", "TRIPTOFANO": "C11H12N2O2",
                   "VALINA": "C5H11NO2", "ALANINA": "C3H7NO2",
                   "GLUTAMATO": "C5H9NO4", "ASPARTATO": "C4H7NO4",
                   "ASPARAGINA": "C4H8N2O3", "ARGININA": "C6H14N4O2",
                   "CISTEINA": "C3H7NO2S", "GLUTAMINA": "C5H10N2O3",
                   "TIROSINA": "C9H11NO3", "GLICINA": "C2H5NO2",
                   "PROLINA": "C5H9NO2", "SERINA": "C3H7NO3"}

    for key in aminoacidos.keys():
        if key == aa1[0].upper():
            if aminoacidos[key] == aa[aa1[0]].upper():
                return True
                break
    return False
