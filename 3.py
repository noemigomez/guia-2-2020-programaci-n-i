#!/usr/bin/env python3
# ! -*- coding:utf-8 -*-
"""
    La lista de aminoácidos que comprueba que lo ingresado sea correcto
    se encuentra en el módulo comprobar.py, presente en la carpeta
    este contiene aminoácidos en español, por lo que si se ingresa
    'Histidine', contará como no válido
"""

import json
import comprobar


# imprime el archivo json
def imprimir():
    with open('lista_aminoacidos.json', 'r') as file:
        print(json.load(file))


# funcion que toma una formula molecular y crea una lista con sus atomos
def listar_atomos(formula):
    atomos = []
    formula = list(formula)
    for i in range(len(formula)):
        veces = 0
        for j in 'CHONS':
            if formula[i].upper() == j:
                atomos.append(j)
            else:
                veces += 1
            if veces == 5:
                atomos[len(atomos) - 1] += formula[i]
    return atomos


# busca cuando se entrega un atomo
def por_atomos(formula, aa, encontrado):
    for key in aa.keys():
        # contador donde todos los atomos deben coincidir, si son 2, veces = 2
        veces = 0
        for i in aa[key]:
            for j in formula:
                if i == j:
                    veces += 1
        if veces == len(formula):
            encontrado.append(key)
    return encontrado


# busca un aa dado una formula
def search(aa, formula):
    encontrado = []
    # transforma los values en listas con los atomos de la formula
    for key in aa.keys():
        aa[key] = listar_atomos(aa[key])
    # si es menor a 4, entonces no es una fórmula completa
    if len(formula) < 4:
        print(len(formula))
        encontrado = por_atomos(formula, aa, encontrado)
    else:
        for key in aa.keys():
            if aa[key] == formula:
                encontrado.append(key)
    print(encontrado)


# abre archivo y pide formula a buscar
def buscar_aa():
    with open('lista_aminoacidos.json') as file:
        aa = json.load(file)
    print(aa)
    try:
        formula = str(input('Por favor, ingrese fórmula o parte de ella: '))
        formula = listar_atomos(formula.upper())
        search(aa, formula)
    except ValueError:
        print('Ingreso no válido, intente nuevamente.')


# cambia la formula del aa
def encontrar_aa(aa, edit):
    for key in aa.keys():
        if key == edit:
            aa[key] = str(input('Formula {0}: '.format(edit))).upper()

    with open('lista_aminoacidos.json', 'w') as file:
        json.dump(aa, file)


# pide que aa va a editar
def editar():
    with open('lista_aminoacidos.json', 'r') as file:
        aa = json.load(file)

    with open('lista_aminoacidoscopia.json', 'w') as file:
        json.dump(aa, file)

    try:
        edit = str(input('Elija que aa va a editar:')).upper()
        encontrar_aa(aa, edit)
    except ValueError:
        print('Lo ingresado no es válido, intente nuevamente.')


# funcion que elimina un aa
def eliminar():
    with open('lista_aminoacidos.json', 'r') as file:
        aa = json.load(file)

    with open('lista_aminoacidoscopia.json', 'w') as file:
        json.dump(aa, file)
    try:
        delete = str(input('Elija que aa va a eliminar:'))
        del aa[delete.upper()]
        with open('lista_aminoacidos.json', 'w') as file:
            json.dump(aa, file)
    except ValueError or KeyError:
        print('Lo ingresado no es válido, intente nuevamente.')


# funcion para editar o eliminar un aa
def ediciones():
    try:
        _choice = int(input('1 (eliminar), 2 (editar) o 0 (salir):'))
        if _choice == 1:
            eliminar()
            ediciones()
        elif _choice == 2:
            editar()
            ediciones()
        elif _choice == 0:
            print('Saliendo')
        else:
            print('Opcion no válida, intente de nuevo.')
            ediciones()
    except ValueError:
        print('Opcion no válida, intente de nuevo.')
        ediciones()


# funcion que crea archivo json con 20 aa y pide ediciones
def agregar_aa():
    aminoacidos = {}
    # se crea diccionario, pasando a json
    while len(aminoacidos) < 4:
        aa = {input('Nombre:').upper(): input('Formula:').upper()}
        # comprueba que el aa ingresado sea correcto
        bien = comprobar.comprobare(aa)
        if bien is True:
            aminoacidos.update(aa)
        else:
            print('Ese aminoácido es incorrecto, siga llenando')

    with open('lista_aminoacidos.json', 'w') as file:
        json.dump(aminoacidos, file)
    # se imprime el archivo json
    imprimir()
    # solicitar si el usuario quiere editar el archivo
    print('Agregó 20 aminoacidos, ¿desea eliminar o editar alguno?')
    edit = str(input('S para sí, cualquier otro caracter para no:'))
    if edit.upper() == 'S':
        ediciones()
    else:
        pass


def menu():
    print('Ingrese opción.')
    print('1.Insertar aminoácidos')
    print('2.Buscar aminoácido por fórmula molecular.')
    print('3. Salir')
    try:
        opcion = int(input())
        if opcion == 1:
            agregar_aa()
            menu()
        elif opcion == 2:
            buscar_aa()
            menu()
        elif opcion == 3:
            imprimir()
        else:
            menu()

    except ValueError:
        menu()


if __name__ == "__main__":
    print('Se presentará un menú, donde el primero sera insertar aminoácidos.')
    print('Si no realiza esto antes, cuando busque no existirá donde.')
    print('Luego de insertar los aminoácidos correctamente:')
    print('Editar o eliminar uno, con el nombre del aminoácido.')
    menu()
